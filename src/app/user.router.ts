import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddModalComponent } from './add-modal/add-modal.component';
import { EditComponent } from './edit/edit.component';
import { GuardService } from "./guard.service";

export const router :Routes=[
      {path: "login", component: LoginComponent},
      {path: "dashboard", component: DashboardComponent,canActivate: [GuardService]},
      {path: "addModal", component: AddModalComponent},
      {path: "edit/:id", component: EditComponent},
      
      {path:"",redirectTo:"login",pathMatch:"full"}
];
export const routes: ModuleWithProviders = RouterModule.forRoot(router);
export const declaration=[
     LoginComponent,
    DashboardComponent,
    AddModalComponent,
    EditComponent,
    AppComponent,
]