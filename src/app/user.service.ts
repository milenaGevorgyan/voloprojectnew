import { Injectable } from '@angular/core';
import { Http,Response, Headers } from '@angular/http';
import { Port } from './user.api';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpService{

    private header = new Headers({'Content-Type' : 'application/json; charset=utf-8'});
    
    constructor( private _http: Http){

    };

    // Check login
    checkLogin(userCheck: Object){
        return this._http.post(Port +"/login",userCheck)
        .map(( res:Response ) =>{
          localStorage.setItem('token', res.json().token),
          res.json();
        });
    }
    // Get Users List
    getUsers(x:number){
        return this._http.get( Port + "/users?page=" + x)
        .map(( res:Response ) => res.json() )
    }

    // Get User
    getUser(ID:number){
        return this._http.get( Port +"/users/"+ ID)
        .map(( res:Response ) => res.json())
    }

    
    // Create User
    createUser(newUser:Object){
        return this._http.post(
                Port + "/users/",
                newUser,
                {headers: this.header}
            ).map((res:Response) => res.json())
    
    }

    // Delete Users
    deleteUser(idUser: number){
        return this._http.delete(  Port + "/users/" + idUser)
        .map((res:Response) => res.json())
    }

    // Update Users
     UpdateObject(upUser: Object){
        return this._http.patch( Port + "/users/",upUser)
        .map((res:Response) => res.json());
        
    }
}