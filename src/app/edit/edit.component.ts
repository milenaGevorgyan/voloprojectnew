import { Component, OnInit } from '@angular/core';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpService } from '../user.service';
import { Router , ParamMap,  } from '@angular/router';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ActivatedRoute }  from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
  providers: [HttpService,DashboardComponent]
})
export class EditComponent implements OnInit {
   
  UpdateObject:{"name":string, "job":string}; 
  sub: any;
  firstname: any;
  lastname: any;
  id: number;
  userInfo = [];
 

  constructor( 
    private update: DashboardComponent,
    private _http: HttpService,
    private router: Router,
    private route: ActivatedRoute ,
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.id = +params['id']; 
    });

    this._http.getUser(this.id)
    .subscribe(
      result => this.userInfo[0] = result.data,
      )   
  };
  // Update users
   updateFunc(nameValue:string, jobValue:string):void{
        this.UpdateObject = {
          "name" : nameValue,
          "job": jobValue
       };
      this.UpdateObj();
       
   }
   
  // Go to Dashboard
  goDash(){
    this.router.navigate(["/dashboard"]);
  }

  //Update User
    UpdateObj(){
        this._http.UpdateObject(this.UpdateObject)
        .subscribe( result =>{
            if( result.updatedAt != ""){
                alert("User is successfully updated");
                this.goDash();         
            }
          });
        }
    }
 
