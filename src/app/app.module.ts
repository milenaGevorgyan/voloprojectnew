import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule  } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {AppComponent} from './app.component'
import { routes, declaration } from './user.router';
import { GuardService } from "./guard.service";
import { ReactiveFormsModule  } from '@angular/forms'

@NgModule({
  declarations: declaration,
   
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule ,
    routes
  ],
  providers: [GuardService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
