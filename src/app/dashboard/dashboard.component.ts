import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpService } from '../user.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AddModalComponent } from '../add-modal/add-modal.component';
import 'rxjs/add/operator/catch';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css'],
	providers: [HttpService],
})

export class DashboardComponent implements OnInit {
	// Varibles
	user: any[];  						 // Users Array
	count = [];							 // Count All Users
	userID: number;
	sortAZ: boolean = true;             // Check sort A-Z

	// Constructor
	constructor(
		private _http: HttpService,
		private router: Router,
	) { }

	// Get Users List
	ngOnInit() {
		this._http.getUsers(1)
			.subscribe(result => {

	// Response result put an array
				this.user = result.data;

	// Formed page array
				for (let i = 1; i <= result.total_pages; i++) {
					this.count[i] = i;
				}
				this.count.shift();
			},
	// Catch Error
			err => alert("Error " + err.status),
		);

	};

	// Get Next List Users
	nextList(x): void {
		this._http.getUsers(x)
			.subscribe(result => this.user = result.data);
	};

	// Create User
	createUser(obj: object) {
		this.user.push(obj);
	}

	// Go to Login Page
	logOut(): void {
		localStorage.removeItem("token");
		this.router.navigate(["/login"]);
	};

	// Edit User
	editUserPage(userIDedite: number) {
		this.router.navigate(["/edit", userIDedite]);
	};

	// Delete User
	sendId(idUse: number): void {
		this.userID = idUse;
	};

	delUser() {
		let deleteResponse: any;

	// Will be work after back-end issue will be done
		this._http.deleteUser(this.userID)
			.subscribe(result => {
				if (result.statusCode == 204) {
					location.reload();
				}
			});
	  };

	// Sort Name
	sortByName(sortBy: string) {
		if (this.sortAZ === true) {
			this.user.sort(this.compare);
			document.getElementById("sortN").style.color = "blue";
			this.sortAZ = false;
		}
		else {
			this.user.sort(this.compare1)
			this.sortAZ = true;
		}
	}
	compare(a, b) {
		if (a.first_name < b.first_name) {
			return -1;
		}
		if (a.first_name > b.first_name) {
			return 1;
		}

	}
	compare1(a, b) {
		if (a.first_name > b.first_name) {
			return -1;
		}
		if (a.first_name < b.first_name) {
			return 1;
		}
	}


	// Sort Last Name
	sortByLastName(sortBy: string) {
		if (this.sortAZ === true) {
			this.user.sort(this.compare2);
			document.getElementById('sortL').style.color = "blue";
			this.sortAZ = false;
		}
		else {
			this.user.sort(this.compare3)
			this.sortAZ = true;
		}
	}
	compare2(a, b) {
		if (a.last_name < b.last_name) {
			return -1;
		}
		if (a.last_name > b.last_name) {
			return 1;
		}

	}
	compare3(a, b) {
		if (a.last_name > b.last_name) {
			return -1;
		}
		if (a.last_name < b.last_name) {
			return 1;
		}
	}
	// Sort ID
	sortId(sortBy: string) {
		if (this.sortAZ === true) {
			this.user.sort(this.compare4);
			document.getElementById("sortR").style.color = "blue";


			this.sortAZ = false;
		}
		else {
			this.user.sort(this.compare5)
			this.sortAZ = true;
		}
	}
	compare4(a, b) {
		if (a.id < b.id) {
			return -1;
		}
		if (a.id > b.id) {
			return 1;
		}
	}
	compare5(a, b) {
		if (a.id > b.id) {
			return -1;
		}
		if (a.id < b.id) {
			return 1;
		}
	}
}
