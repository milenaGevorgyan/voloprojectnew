import { Component, OnInit } from '@angular/core';
import { HttpService } from '../user.service';
import { Routes, Router, RouterModule, } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';

@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.css'],
  providers: [DashboardComponent],
})
export class AddModalComponent implements OnInit {	
		userObject: any = { "first_name": "", "last_name": "", "avatar": "", };
	constructor(
		private dash: DashboardComponent,
		private router:Router,
		private _http: HttpService
	) { }

	ngOnInit() {

	};

 addUser(userName:string,userLast:string):void{
			this.userObject = {
				"first_name": userName,
				"last_name": userLast,
				"avatar": ""
			};	
    		this.createUserCheck(this.userObject);
 }
	createUserCheck(obj){
         if(this.userObject.first_name != "" && this.userObject.last_name != ""){
			this._http.createUser(obj)
			.subscribe( result =>{
					alert("User is successfully Created");
					document.getElementById("close_popUp").click();
			});
		}
		else{
			alert("Please fill input fildes")
		}
	}
}
