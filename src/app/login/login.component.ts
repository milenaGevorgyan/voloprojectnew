import { Component, OnInit } from '@angular/core';
import { HttpService } from "../user.service";
import { NgForm, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { User } from '../app.bodyLogin';
import { GlobalValidator } from "../directive.validation";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [HttpService]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    mailAddress: FormControl;
    passValue: FormControl;

    // Cunstructor
    constructor(
        private _http: HttpService,
        private router: Router,
        private build: FormBuilder,

    ) {
        this.mailAddress = new FormControl("", GlobalValidator.mailFormat);
        this.passValue = new FormControl("", GlobalValidator.PassFormat);
        this.loginForm = build.group({
            mailAddress: this.mailAddress,
            passValue: this.passValue,
        })
    };

    // Varibles
    emailUser: string;  // User Email Varible
    passUser: any;      // User Password Varible
    checkLogin: Object; // Object users information
     
    ngOnInit() {

    }

    // Get User Info
    loginFunc(loginValue: string, passValue: any) {

    // Object users information
        this.checkLogin = {
            "email": loginValue,
            "password": passValue
        }

     // Login Check Request
        this._http.checkLogin(this.checkLogin)
            .subscribe(
            result => {
                if (localStorage.getItem('token')) {
                    this.router.navigate(["/dashboard"]);
                }
            },
     // Catch Error
            err => alert(err._body),
        );
    }
}
